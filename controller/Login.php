<?php

require_once 'lib/Controller.php';
//require_once 'model/LoginModel.php';

class Login extends Controller
{

    function __construct()
    {
        parent::__construct('Login');
    }

    public function index()
    {
        $this->login();
    }
    public function login()
    {
        $this->view->render();
    }

    public function run()
    {
        $user = $_POST['name'];
        $password = $_POST['password'];
        if ($this->model->validate($user, $password)) {
//            $_SESSION['user'] = $user;
            header('Location: ' . Config::URL . $_SESSION['lang'] . '/index');
        } else {
            $this->view->setMessage('Login incorrecto. Inténtelo de nuevo');
            $this->index();
        }
    }

    public function logout()
    {
        unset($_SESSION);
        session_destroy();
        header('location: ' . Config::URL . 'login');
        exit;
    }

}
