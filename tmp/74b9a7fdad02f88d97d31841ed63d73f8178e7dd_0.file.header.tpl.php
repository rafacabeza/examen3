<?php /* Smarty version 3.1.27, created on 2016-03-02 17:40:55
         compiled from "C:\wamp\www\examen2\template\header.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:828556d71797638172_81246984%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '74b9a7fdad02f88d97d31841ed63d73f8178e7dd' => 
    array (
      0 => 'C:\\wamp\\www\\examen2\\template\\header.tpl',
      1 => 1456936852,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '828556d71797638172_81246984',
  'variables' => 
  array (
    'language' => 0,
    'url' => 0,
    'js' => 0,
    'file' => 0,
    'lang' => 0,
    'user' => 0,
    'login' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56d717976b2292_91956907',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56d717976b2292_91956907')) {
function content_56d717976b2292_91956907 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '828556d71797638172_81246984';
?>
<!DOCTYPE html>

<html>
    <head>
        <title><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('app_title');?>
</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
public/css/default.css" />
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
public/js/jquery.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php if (isset($_smarty_tpl->tpl_vars['js']->value) && count($_smarty_tpl->tpl_vars['js']->value)) {?>
        <?php
$_from = $_smarty_tpl->tpl_vars['js']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['file'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['file']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['file']->value) {
$_smarty_tpl->tpl_vars['file']->_loop = true;
$foreach_file_Sav = $_smarty_tpl->tpl_vars['file'];
?>        
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
public/js/<?php echo $_smarty_tpl->tpl_vars['file']->value;?>
" type="text/javascript"><?php echo '</script'; ?>
>        
        <?php
$_smarty_tpl->tpl_vars['file'] = $foreach_file_Sav;
}
?>   
        <?php }?>
         
    </head>
    <body>
        <div id="header">
            <div id="title">
                <?php echo $_smarty_tpl->tpl_vars['language']->value->translate('app_title');?>

            </div>
            <div  style="float: left">
                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/index" ><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('index');?>
</a> 
                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/pet" ><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('pet');?>
</a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/pet/report" >Lista de visitas</a>
            </div>

            <div  style="float: right ; padding-left:4em">
                <?php echo $_smarty_tpl->tpl_vars['user']->value;?>

                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/login/<?php echo $_smarty_tpl->tpl_vars['login']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['login']->value;?>
</a>
            </div>
        </div>

            
<?php }
}
?>