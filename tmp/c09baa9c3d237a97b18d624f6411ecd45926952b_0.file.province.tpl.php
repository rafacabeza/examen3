<?php /* Smarty version 3.1.27, created on 2016-03-11 10:32:37
         compiled from "template\province.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:2235156e290b58a3d56_44495024%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c09baa9c3d237a97b18d624f6411ecd45926952b' => 
    array (
      0 => 'template\\province.tpl',
      1 => 1457688752,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2235156e290b58a3d56_44495024',
  'variables' => 
  array (
    'language' => 0,
    'rows' => 0,
    'row' => 0,
    'url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56e290b5916461_81386679',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56e290b5916461_81386679')) {
function content_56e290b5916461_81386679 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '2235156e290b58a3d56_44495024';
echo $_smarty_tpl->getSubTemplate ("template/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"encabezado"), 0);
?>

<div id="content">
    <h2><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('province_list');?>
</h2>

    <table><thead>
        <tr>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('codigo');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('provincia');?>
</th>
        </tr>
        </thead>
        <tbody id="tbodyList">
        <?php
$_from = $_smarty_tpl->tpl_vars['rows']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['row'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['row']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
$foreach_row_Sav = $_smarty_tpl->tpl_vars['row'];
?>
        <tr >
            <td class="cell"><?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
 </td>
            <td class="cell"><a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
town/index/<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
" ><?php echo $_smarty_tpl->tpl_vars['row']->value['provincia'];?>
 </a></td>
        </tr>        
        <?php
$_smarty_tpl->tpl_vars['row'] = $foreach_row_Sav;
}
?>
        </tbody>        
    </table>
        

        
        
    
   
    <div id="pageIndex"> indice de paginas</div>


 </div>
<?php echo $_smarty_tpl->getSubTemplate ("template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"footer"), 0);

}
}
?>