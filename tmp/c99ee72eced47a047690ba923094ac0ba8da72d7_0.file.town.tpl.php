<?php /* Smarty version 3.1.27, created on 2016-03-11 01:36:40
         compiled from "template\town.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:2425856e213182c52e7_04730697%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c99ee72eced47a047690ba923094ac0ba8da72d7' => 
    array (
      0 => 'template\\town.tpl',
      1 => 1457656598,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2425856e213182c52e7_04730697',
  'variables' => 
  array (
    'language' => 0,
    'province' => 0,
    'pages' => 0,
    'url' => 0,
    'i' => 0,
    'rows' => 0,
    'row' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56e2131837e3b9_58714569',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56e2131837e3b9_58714569')) {
function content_56e2131837e3b9_58714569 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '2425856e213182c52e7_04730697';
echo $_smarty_tpl->getSubTemplate ("template/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"encabezado"), 0);
?>

<div id="content">
    <h2><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('town_list');?>
:   <?php echo $_smarty_tpl->tpl_vars['province']->value['provincia'];?>
</h2>
    <p>
    <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? $_smarty_tpl->tpl_vars['pages']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['pages']->value)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
        <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
town/index/<?php echo $_smarty_tpl->tpl_vars['province']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</a>
        
    <?php }} ?>
    </p>
    <table><thead>
        <tr>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('codigo');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('poblacion');?>
</th>
        </tr>
        </thead>
        <tbody id="tbodyList">
        <?php
$_from = $_smarty_tpl->tpl_vars['rows']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['row'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['row']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
$foreach_row_Sav = $_smarty_tpl->tpl_vars['row'];
?>
        <tr >
            <td class="cell"><?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
 </td>
            <td class="cell"><?php echo $_smarty_tpl->tpl_vars['row']->value['poblacion'];?>
 </td>
        </tr>        
        <?php
$_smarty_tpl->tpl_vars['row'] = $foreach_row_Sav;
}
?>
        </tbody>        
    </table>
        

        
        
        
    
   
    <div id="pageIndex"> indice de paginas</div>


 </div>
<?php echo $_smarty_tpl->getSubTemplate ("template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"footer"), 0);

}
}
?>