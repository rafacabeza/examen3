<?php /* Smarty version 3.1.27, created on 2016-03-11 15:39:43
         compiled from "C:\wamp\www\examen3\template\header.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:105056e2d8afe04e13_05056693%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd7c4e1300cfa5f1e4b4b419cc0c64a488c3308c4' => 
    array (
      0 => 'C:\\wamp\\www\\examen3\\template\\header.tpl',
      1 => 1457707181,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '105056e2d8afe04e13_05056693',
  'variables' => 
  array (
    'language' => 0,
    'url' => 0,
    'js' => 0,
    'file' => 0,
    'lang' => 0,
    'user' => 0,
    'login' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56e2d8afeb08a4_82180978',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56e2d8afeb08a4_82180978')) {
function content_56e2d8afeb08a4_82180978 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '105056e2d8afe04e13_05056693';
?>
<!DOCTYPE html>

<html>
    <head>
        <title><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('app_title');?>
</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
public/css/default.css" />
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
public/js/jquery.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php if (isset($_smarty_tpl->tpl_vars['js']->value) && count($_smarty_tpl->tpl_vars['js']->value)) {?>
        <?php
$_from = $_smarty_tpl->tpl_vars['js']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['file'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['file']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['file']->value) {
$_smarty_tpl->tpl_vars['file']->_loop = true;
$foreach_file_Sav = $_smarty_tpl->tpl_vars['file'];
?>        
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
public/js/<?php echo $_smarty_tpl->tpl_vars['file']->value;?>
" type="text/javascript"><?php echo '</script'; ?>
>        
        <?php
$_smarty_tpl->tpl_vars['file'] = $foreach_file_Sav;
}
?>   
        <?php }?>
         
    </head>
    <body>
        <div id="header">
            <div id="title">
                <?php echo $_smarty_tpl->tpl_vars['language']->value->translate('app_title');?>

            </div>
            <div  style="float: left">
                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/index" ><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('index');?>
</a> 
                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/province" ><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('province');?>
</a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/town" ><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('town');?>
</a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/town/search" ><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('search');?>
</a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/town/townlist" ><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('list');?>
</a>
           </div>

            <div  style="float: right ; padding-left:4em">
                <?php echo $_smarty_tpl->tpl_vars['user']->value;?>

                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/login/<?php echo $_smarty_tpl->tpl_vars['login']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['login']->value;?>
</a>
            </div>
        </div>

            
<?php }
}
?>